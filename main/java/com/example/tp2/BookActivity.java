package com.example.tp2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        Intent intent = getIntent();
        final Book book = intent.getParcelableExtra("book");
        final int requestCode = intent.getIntExtra("requestCode", -1);

        final EditText nameBook = findViewById(R.id.nameBook);
        nameBook.setText(book.getTitle());

        final EditText editAuthors = findViewById(R.id.editAuthors);
        editAuthors.setText(book.getAuthors());

        final EditText editYear = findViewById(R.id.editYear);
        editYear.setText(book.getYear());

        final EditText editGenres = findViewById(R.id.editGenres);
        editGenres.setText(book.getGenres());

        final EditText editPublisher = findViewById(R.id.editPublisher);
        editPublisher.setText(book.getPublisher());

        Button saveButton = findViewById(R.id.button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nameBook.getText().toString().equals("")) {
//                    Toast.makeText(BookActivity.this, "Le nom du livre doit être non vide", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder builder = new AlertDialog.Builder(BookActivity.this);
                    builder.setTitle("Sauvegarde impossible").setMessage("Le nom du livre doit être non vide").show();
                    return;
                }
                book.setTitle(nameBook.getText().toString());
                book.setAuthors(editAuthors.getText().toString());
                book.setYear(editYear.getText().toString());
                book.setGenres(editGenres.getText().toString());
                book.setPublisher(editPublisher.getText().toString());
                BookDbHelper bookDbHelper = new BookDbHelper(BookActivity.this);
                switch (requestCode) {
                    case 1:
                        bookDbHelper.updateBook(book);
                        break;
                    case 2:
                        bookDbHelper.addBook(book);
                        break;
                }
                finish();
            }
        });
    }
}
