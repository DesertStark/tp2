package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.tp2.BookDbHelper.cursorToBook;


public class MainActivity extends AppCompatActivity {

    private SimpleCursorAdapter cursorAdapter;
    private BookDbHelper bookDbHelper;

    Book[] getBooks() {
        Book[] books = {
                new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"),
                new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"),
                new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"),
                new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"),
                new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"),
                new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"),
                new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"),
                new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"),
                new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"),
                new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"),
                new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"),
        };
        return books;
    }

    String[] getBooksTitle() {
        ArrayList<String> booksTitle = new ArrayList<String>();
        for (Book book : getBooks()) {
            booksTitle.add(book.getTitle());
        }
        return booksTitle.toArray(new String[booksTitle.size()]);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView booksListView = findViewById(R.id.booksList);

        bookDbHelper = new BookDbHelper(this);
        bookDbHelper.populate();
        cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                bookDbHelper.fetchAllBooks(),
                new String[]{BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        booksListView.setAdapter(cursorAdapter);
        booksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", cursorToBook((Cursor) parent.getItemAtPosition(position)));
                intent.putExtra("requestCode", 1);
                startActivityForResult(intent, 1);
            }
        });

        final FloatingActionButton addActionButton = findViewById(R.id.addActionButton);
        addActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book newBook = new Book(null, null, null ,null, null);
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", newBook);
                intent.putExtra("requestCode", 2);
                startActivityForResult(intent, 2);
            }
        });

//        registerForContextMenu(booksListView);

        booksListView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add("Supprimer");
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        View v = menuInfo.targetView;
        Cursor bookCursor = (Cursor) cursorAdapter.getItem(menuInfo.position);
        bookDbHelper.deleteBook(bookCursor);
        cursorAdapter.changeCursor(bookDbHelper.fetchAllBooks());
        cursorAdapter.notifyDataSetChanged();
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cursorAdapter.changeCursor(bookDbHelper.fetchAllBooks());
        cursorAdapter.notifyDataSetChanged();
    }
}
